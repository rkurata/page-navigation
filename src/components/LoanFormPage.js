import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { activityManagerHelper } from '@fieldlink/react-workflow-manager';
import { Typography, TextField, Button } from '@material-ui/core';
import { submitLoanForm } from '../services/pagesServices';
import Page from './Page';

import './loanformPage.css';

const LoanFormPage = ({ currentProcess, activityStatus }) => {
    const loanForm = React.createRef();
    const submitForm = () => {
        const inputFields = {}
        for (const element of loanForm.current.elements) {
            if (element.nodeName === 'INPUT') {
                inputFields[element.name] = element.value;
            }
        }

        submitLoanForm(inputFields, currentProcess);
    }
    return (
        // currentProcess ?
        <Page>
            <div className="loanFormPage">
                <Typography component="h2" variant="h5" align="left" >Informe o valor desejado</Typography>
                <form ref={loanForm}>
                    <TextField name="userValue" variant="outlined" margin="normal" fullWidth={true} label="Valor" type="number"></TextField>
                    <div className="loanFormPage__buttons">
                        <Button variant="contained" color="primary" onClick={() => submitForm()}>ENVIAR</Button>
                    </div>
                </form>
                {
                    activityStatus === 'completed'
                        ?
                        <div className="overlay">
                            <Typography component="p" variant="h5" color="inherit">Em análise...</Typography>
                        </div>
                        :
                        null
                }
            </div>
        </Page>
        // :
        // <Redirect to="/"/>
    )
}

function mapStateToProps(state) {
    const groupedActivities = activityManagerHelper.groupByProcess(state.workflow.activityManagers);
    let activityStatus;
    const activityList = groupedActivities[state.page.currentProcess];
    if (activityList && activityList.length > 0) {
        const currentActivity = activityList[0];
        if (currentActivity && currentActivity.props.action === 'valuePage') {
            activityStatus = currentActivity.activity_status;
        }
    }
    return {
        currentProcess: state.page.currentProcess,
        activityStatus: activityStatus,
    }
}

export default connect(mapStateToProps)(LoanFormPage);