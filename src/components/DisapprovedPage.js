import React from 'react';
import { connect } from 'react-redux';
import { activityManagerHelper } from '@fieldlink/react-workflow-manager';
import { Typography } from '@material-ui/core';
import Page from './Page';

import './disapprovedPage.css';

const DisapprovedPage = ({requestedValue}) => {
    return (
        <Page>
            <div className="disapprovedPage">
                <Typography component="p" variant="h5">O seu empréstimo, no valor de R$ {requestedValue}, não foi aprovado.</Typography>
            </div>
        </Page>
    )
}

function mapStateToProps(state) {
    const groupedActivities = activityManagerHelper.groupByProcess(state.workflow.activityManagers);
    let requestedValue = 0;
    const activityList = groupedActivities[state.page.currentProcess];
    if (activityList && activityList.length > 0) {
        const currentActivity = activityList[0];
        if (currentActivity && currentActivity.props.action === 'disapproved') {
            requestedValue = currentActivity.props.result.userValue
        }
    }
    return {
        requestedValue: requestedValue
    }
}

export default connect(mapStateToProps)(DisapprovedPage);