import React from 'react';
import { connect } from 'react-redux';
import { activityManagerHelper } from '@fieldlink/react-workflow-manager';
import { Typography } from '@material-ui/core';
import Page from './Page';

import './approvedPage.css';

const ApprovedPage = ({approvedValue}) => {
    return (
        <Page>
            <div className="approvedPage">
                <Typography component="h2" variant="h3" align="center" color="primary">PARABÉNS</Typography>
                <Typography component="p" variant="h5">O seu empréstimo, no valor de R$ {approvedValue}, foi aprovado</Typography>
            </div>
        </Page>
    )
}

function mapStateToProps(state) {
    const groupedActivities = activityManagerHelper.groupByProcess(state.workflow.activityManagers);
    let approvedValue = 0;
    const activityList = groupedActivities[state.page.currentProcess];
    if (activityList && activityList.length > 0) {
        const currentActivity = activityList[0];
        if (currentActivity && currentActivity.props.action === 'approved') {
            approvedValue = currentActivity.props.result.userValue
        }
    }
    return {
        approvedValue: approvedValue
    }
}

export default connect(mapStateToProps)(ApprovedPage);