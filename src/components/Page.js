import React from 'react';
import { NavLink } from 'react-router-dom';
import { Link } from '@material-ui/core';
import routes from '../routes';

import './page.css';
import Logo from './Logo';

const Page = ({ children }) => {
    return (
        <div className="page">
            <div className="page__navigation">
                {
                    routes.map((route) => <div key={route.path}><Link component={NavLink} exact to={route.path}>{route.label}</Link></div>)
                }
            </div>
            <div className="page__card">
                <Logo></Logo>
                {children}
            </div>
        </div>
    )
}

export default Page;