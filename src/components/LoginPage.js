import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Typography, TextField, Button, withStyles } from "@material-ui/core"
import { submitLoginForm } from '../services/pagesServices';
import Page from './Page';

import './loginPage.css';

const LoginPage = ({ classes, currentProcess }) => {
    const submitForm = (event) => {
        const inputFields = {}
        for (const element of event.target) {
            if (element.nodeName === 'INPUT') {
                inputFields[element.name] = element.value;
            }
        }

        submitLoginForm(inputFields, currentProcess);
        event.preventDefault();
    }
    return (
        // currentProcess ?
        <Page>
            <div className="loginPage">
                <Typography component="h2" variant="h5" align="left" classes={{ root: classes.title }}>ACESSE A SUA CONTA</Typography>
                <form className="loginForm" onSubmit={(event) => submitForm(event)}>
                    <TextField name="login" required variant="outlined" margin="normal" fullWidth={true} label="Email ou CPF"></TextField>
                    <TextField name="password" required variant="outlined" margin="normal" fullWidth={true} type="password" label="Senha de acesso"></TextField>
                    <div className="loginForm__submit">
                        <Button variant="contained" type="submit" classes={{ root: classes.successButton }} >ENTRAR</Button>
                    </div>
                </form>
            </div>
        </Page>
        // :
        // <Redirect to="/"/>
    )
}

function createStyles(theme) {
    return {
        successButton: {
            'background-color': theme.palette.success.main,
            '&:hover': {
                'background-color': theme.palette.success.dark,
            },
            color: theme.palette.common.white,
        },
        title: {
            'margin-bottom': '15px'
        }
    }
}

function mapStateToProps(state) {
    return {
        currentProcess: state.page.currentProcess,
    }
}

export default connect(mapStateToProps)(withStyles(createStyles)(LoginPage));