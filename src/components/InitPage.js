import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import Page from './Page';
import { Button, Typography } from '@material-ui/core';
import { init } from '../services/pagesServices';
import { setProcess } from '../redux/pageActions';
import { requestSpecificToken } from '../services/permissionService';
import { listActivityManagers } from '../services/workflowService';

import './initPage.css';
import { savePendingApproval } from '../redux/mesaActions';

const InitPage = ({ selectProcess, savePendingApproval, history }) => {
    const initPages = async () => {
        await requestSpecificToken(['client']);
        const processId = await init();
        selectProcess(processId)
    }
    const initMesa = async () => {
        await requestSpecificToken(['manager']);
        const activityManagers = await listActivityManagers();
        for (const activityManger of activityManagers) {
            if (activityManger.props.action === 'manualApproval') {
                savePendingApproval({
                    id: activityManger.id,
                    ...activityManger.props,
                    processId: activityManger.process_id
                });
            }
        }
        history.push('/pendingApproval');
    }
    return (
        <Page>
            <div className="initPage">
                <Typography component="h2" variant="h3">Iniciar</Typography>
                <div className="buttonList">
                    <Button variant="contained" color="primary" onClick={() => initPages()} className="button" >Cliente</Button>
                    <Button variant="contained" color="primary" onClick={() => initMesa()} className="button" >Mesa</Button>
                </div>
            </div>
        </Page>
    )
}

function mapDispatchToProps(dispatch) {
    return {
        selectProcess: bindActionCreators(setProcess, dispatch),
        savePendingApproval: bindActionCreators(savePendingApproval, dispatch),
    }
}

export default connect(null, mapDispatchToProps)(InitPage)