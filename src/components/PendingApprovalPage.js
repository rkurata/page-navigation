import React from 'react';
import { connect } from 'react-redux';
import { Typography, Button } from '@material-ui/core';
import Page from './Page';
import './pendingApprovalPage.css'
import { submitEvaluation } from '../services/pagesServices';

const PendingApprovalPage = ({ pendingApproval }) => {
    const submit = async (evaluation, processId) => {
        submitEvaluation(evaluation, processId);
    }
    return (
        <Page>
            <div className="pendingApprovalPage">
                {
                    pendingApproval.length === 0
                        ?
                        <div className="pendingApprovalPage__emptyMessage">
                            <Typography component="p" variant="h5">Nenhuma aprovação pendente</Typography>
                        </div>
                        :
                        <div className="pendingApprovalPage__pendingList">
                            <Typography component="h3" variant="h4">Pedidos pendentes</Typography>
                            <div className="list">
                                {
                                    pendingApproval.map((activityManager) => (
                                        <div className="pendingApproval list__item page__card" key={activityManager.id}>
                                            <Typography component="p" variant="body1">
                                                Pedido de R$ {activityManager.result.userValue} do usuário {activityManager.result.login}
                                            </Typography>
                                            <div className="pendingApproval__buttons buttonList" >
                                                <Button variant="contained" color="primary" className="button" onClick={() => submit('approved', activityManager.processId)} >Aprova</Button>
                                                <Button variant="contained" color="secondary" className="button" onClick={() => submit('disapproved', activityManager.processId)} >Reprova</Button>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                        </div>
                }
            </div>
        </Page>
    )
}

function mapStateToProps(state) {
    return {
        pendingApproval: Object.values(state.mesa.pendingApproval),
    }
}

export default connect(mapStateToProps)(PendingApprovalPage);