import React from 'react';

import './logo.css'

const Logo = () => {
    return (
        <div className="logo">
            <img src="/lendico-logo.svg" alt="Lendico" className="logo__img"></img>
        </div>
    )
}

export default Logo;