import React from 'react';
import { connect } from 'react-redux';
import { WorkflowManager } from '@fieldlink/react-workflow-manager';
import { Router, Route } from 'react-router-dom';
import history from '../history';
import routes from '../routes';

function App({ status, currentPage }) {
  return (
    <WorkflowManager>
      <Router history={history}>
        {
          routes.map((route) => <Route key={route.path} exact path={route.path} component={route.component} />)
        }
      </Router>
    </WorkflowManager>
  );
}

export default App;
