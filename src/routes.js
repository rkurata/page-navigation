import InitPage from "./components/InitPage";
import LoginPage from "./components/LoginPage";
import LoanFormPage from "./components/LoanFormPage";
import ApprovedPage from "./components/ApprovedPage";
import DisapprovedPage from "./components/DisapprovedPage";
import PendingApprovalPage from "./components/PendingApprovalPage";

export default [
    {
        path: '/',
        label: 'Init',
        component: InitPage,
    },
    {
        path: '/login',
        label: 'Login',
        component: LoginPage,
    },
    {
        path: '/loanform',
        label: 'LoanForm',
        component: LoanFormPage,
    },
    {
        path: '/approved',
        label: 'Approved',
        component: ApprovedPage,
    },
    {
        path: '/disapproved',
        label: 'Disapproved',
        component: DisapprovedPage,
    },
    {
        path: '/pendingApproval',
        label: 'PendingApproval',
        component: PendingApprovalPage,
    },
]