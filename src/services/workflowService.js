import { getJWTToken } from './permissionService';

export async function createProcess(workflowName) {
    const token = await getJWTToken();
    const response = await fetch(`${process.env.REACT_APP_BACK_BASE_URL}/workflow/${workflowName}/process`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    });

    if (response.ok) {
        const json = await response.json();
        return json;
    }
}

export async function runProcess(processId) {
    const token = await getJWTToken();
    const response = await fetch(`${process.env.REACT_APP_BACK_BASE_URL}/workflow/process/${processId}`, {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    });

    return response.ok;
}

export async function getProcess(processId) {
    const response = await fetch(`${process.env.REACT_APP_BACK_BASE_URL}/workflow/process/${processId}`);

    if (response.ok) {
        const json = await response.json();
        return json;
    }
}

export async function listActivityManagers() {
    const token = await getJWTToken();
    const response = await fetch(`${process.env.REACT_APP_BACK_BASE_URL}/workflow/process/activityManager/list`, {
        headers: {
            Authorization: `Bearer ${token}`,
        }
    });

    if (response.ok) {
        const json = await response.json();
        return json;
    } else {
        return {
            error: true,
            status: response.status,
        };
    }
}

export async function getActivityManager(activityManagerId) {
    const token = await getJWTToken();
    const response = await fetch(`${process.env.REACT_APP_BACK_BASE_URL}/workflow/process/activityManager/${activityManagerId}`, {
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    });

    if (response.ok) {
        const json = await response.json();
        return json;
    }
    return {
        error: true,
        status: response.status,
    }
}

export async function commitActivity(processId, activity) {
    const token = await getJWTToken();
    const response = await fetch(`${process.env.REACT_APP_BACK_BASE_URL}/workflow/process/${processId}/activityManager/activity/commit`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            input: activity
        })
    })

    return response.ok;
}

export async function pushActivity(processId) {
    const token = await getJWTToken();
    const response = await fetch(`${process.env.REACT_APP_BACK_BASE_URL}/workflow/process/${processId}/activityManager/activity/push`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    })

    if (response.ok) {
        const json = await response.json();
        return json;
    }
}

export async function submitActivity(processId, activity) {
    const token = await getJWTToken();
    const response = await fetch(`${process.env.REACT_APP_BACK_BASE_URL}/workflow/process/${processId}/activityManager/activity/submit`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            input: activity
        })
    })

    if (response.ok) {
        const json = await response.json();
        return json;
    }
}