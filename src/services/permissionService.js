const actorData = {
    id: (((Math.random() * 100).toFixed() % 7) + 1).toString(),
    claims: []
}
export function getActorData() {
    return actorData;
}


let token;
export async function requestSpecificToken(claims) {
    const response = await fetch(`${process.env.REACT_APP_BACK_BASE_URL}/permission/${actorData.id}/token`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            claims: claims,
        })
    });

    if (response.ok) {
        const json = await response.json();
        token = json.token;
    } else {
        console.error(`Error requesting token, ${response}`);
    }
}

export async function getJWTToken() {
    if (!token) {
        await requestSpecificToken(['client']);
    }
    return token;
}