import { createProcess, runProcess, submitActivity } from "./workflowService";


export async function init() {
    const process = await createProcess('pageExample');
    await runProcess(process.id);
    return process.id;
}

export async function submitLoginForm(loginForm, currentProcess) {
    console.log(loginForm);
    console.log(currentProcess);
    if (currentProcess) {
        await submitActivity(currentProcess, loginForm)
    }
}

export async function submitLoanForm(loanForm, currentProcess) {
    console.log(loanForm);
    console.log(currentProcess);
    if (currentProcess) {
        await submitActivity(currentProcess, loanForm);
    }
}

export async function submitEvaluation(evaluation, currentProcess) {
    console.log(evaluation);
    if (currentProcess) {
        await submitActivity(currentProcess, {evaluation: evaluation});
    }
}