export const MANUAL_APPROVAL = 'WF/manualApproval';
export const COMPLETED_MANUAL_APPROVAL = 'WF/manualApproval/completed'

export const SAVE_PENDING_APPROVAL = 'SAVE_PENDING_APPROVAL';