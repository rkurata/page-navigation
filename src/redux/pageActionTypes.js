export const LOGIN_PAGE = 'WF/loginPage';
export const VALUE_PAGE = 'WF/valuePage';
export const APPROVED = 'WF/approved';
export const DISAPPROVED = 'WF/disapproved';
export const CHANGE_ROUTE = 'CHANGE_ROUTE';
export const SET_PROCSES = 'SET_PROCSES'