import { SET_PROCSES } from "./pageActionTypes";

export function setProcess(processId) {
    return {
        type: SET_PROCSES,
        payload: processId,
    };
}