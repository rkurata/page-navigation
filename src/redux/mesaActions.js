import { SAVE_PENDING_APPROVAL } from "./mesaActionTypes";

export function savePendingApproval(activityManager) {
    return {
        type: SAVE_PENDING_APPROVAL,
        payload: activityManager,
    };
}