import { workflowSagas } from '@fieldlink/react-workflow-manager';

function* init() {
    yield workflowSagas()
}

export default init;