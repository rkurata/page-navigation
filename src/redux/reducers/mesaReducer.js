import { SAVE_PENDING_APPROVAL, MANUAL_APPROVAL, COMPLETED_MANUAL_APPROVAL } from "../mesaActionTypes";

const initialState = {
    pendingApproval: {},
}

export default function mesa(state = initialState, action) {
    switch (action.type) {
        case MANUAL_APPROVAL: {
            const resultState = {
                ...state,
            };

            resultState.pendingApproval[action.payload.id] = action.payload;

            return resultState;
        }
        case COMPLETED_MANUAL_APPROVAL: {
            const resultState = {
                ...state,
            };

            delete resultState.pendingApproval[action.payload.id];

            return resultState;
        }
        case SAVE_PENDING_APPROVAL: {
            const resultState = {
                ...state,
            };

            resultState.pendingApproval[action.payload.id] = action.payload;

            return resultState;
        }
        default:
            return state;
    }
}