import history from '../../history';
import { LOGIN_PAGE, VALUE_PAGE, APPROVED, DISAPPROVED, SET_PROCSES } from '../pageActionTypes';

let currentProcess = null;

const urlQuery = new URLSearchParams(window.location.search);
const processQuery = urlQuery.get('p');
if (processQuery) {
    currentProcess = processQuery.split(';')[0];
}
const initialState = {
    currentProcess: currentProcess,
};

export default function page(state = initialState, action) {
    let destinyPath;
    switch (action.type) {
        case LOGIN_PAGE: {
            destinyPath = '/login';
            break;
        }
        case VALUE_PAGE: {
            destinyPath = '/loanForm'
            break;
        }
        case APPROVED: {
            destinyPath = '/approved'
            break;
        }
        case DISAPPROVED: {
            destinyPath = '/disapproved'
            break;
        }
        case SET_PROCSES: {
            return {
                ...state,
                currentProcess: action.payload,
            };
        }
        default: {
            break;
        }
    }
    if (destinyPath && action.payload.processId === state.currentProcess) {
        setTimeout(
            () => {
                history.push(destinyPath);
            },
            0
        );
    }
    return state;
}