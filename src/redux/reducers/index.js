import { workflowReducer } from '@fieldlink/react-workflow-manager';
import pageReducer from './pageReducer';
import mesaReducer from './mesaReducer';

export default (state = {}, action) => {
    return {
        workflow: workflowReducer(state.workflow, action),
        page: pageReducer(state.page, action),
        mesa: mesaReducer(state.mesa, action),
    }
}